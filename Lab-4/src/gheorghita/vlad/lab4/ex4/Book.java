package gheorghita.vlad.lab4.ex4;

import gheorghita.vlad.lab4.ex2.Author;

import java.util.Arrays;

public class Book {
    private String name;
    private Author[] author;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.price = price;
        author = authors;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        author = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author=" + Arrays.toString(author) +
                ", price=" + price +
                ", qtyInStock=" + qtyInStock +
                '}';
    }

    public void printAuthors() {
        System.out.println(Arrays.toString(author));
    }
}
