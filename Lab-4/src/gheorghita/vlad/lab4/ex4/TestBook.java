package gheorghita.vlad.lab4.ex4;

import gheorghita.vlad.lab4.ex2.Author;

import java.util.Arrays;

public class TestBook {
    public static void main(String[] args) {

        Author[] aut = new Author[3];
        aut[0] = new Author("Vasile", "vasile@gmail.com", 'm');
        aut[1] = new Author("Mircea", "mircea@gmail.com", 'm');
        aut[2] = new Author("Felicia", "felicia@yahoo.com", 'f');
        Book b = new Book("THE BOOK", aut, 200, 67);
        System.out.println(b.toString() + "\n");
        b.setPrice(2);
        b.setQtyInStock(500);
        b.printAuthors();
        System.out.println(Arrays.toString(b.getAuthor()) + " " + b.getName() + " " + b.getPrice() + " " + b.getQtyInStock());
    }
}

