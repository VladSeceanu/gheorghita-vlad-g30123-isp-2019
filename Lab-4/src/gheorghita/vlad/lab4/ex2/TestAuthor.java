package gheorghita.vlad.lab4.ex2;

public class TestAuthor {
    public static void main(String[] args){
        Author autor=new Author("Mihai","mihai@gmail.com",'m');
        System.out.println("Old Email: "+autor.getEmail());
        autor.setEmail("mihai@yahoo.com");
        System.out.println("New Email: "+autor.getEmail());
        System.out.println("Name and gender by methods getName() and getGender: name:"+autor.getName()+" gender: " + autor.getGender());
        System.out.println("Name, email and gender with method toString(): " + autor.toString());
    }
}
