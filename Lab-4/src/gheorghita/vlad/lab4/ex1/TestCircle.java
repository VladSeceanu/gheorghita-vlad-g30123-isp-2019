package gheorghita.vlad.lab4.ex1;

public class TestCircle {
    public static void main(String[] args){
        Circle cerc1=new Circle();
        Circle cerc2=new Circle(2.5);
        System.out.println("cerc1 radius: "+cerc1.getRadius()+" cerc1 Area: "+cerc1.getArea());
        System.out.println("cerc2 radius: "+cerc2.getRadius()+" cerc2 Area: "+cerc2.getArea());
    }
}
