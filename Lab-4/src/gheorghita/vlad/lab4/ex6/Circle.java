package gheorghita.vlad.lab4.ex6;

import static java.lang.Math.PI;

public class Circle extends Shape {
    private double radius = 1.0;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean fiiled, double radius) {
        super(color, fiiled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return PI * radius * radius;
    }

    public double getPerimeter() {
        return 2 * PI * radius;
    }


    public String toString() {
        return "A Circle with radius= " + radius + ", which is a subclass of " + super.toString();
    }
}
