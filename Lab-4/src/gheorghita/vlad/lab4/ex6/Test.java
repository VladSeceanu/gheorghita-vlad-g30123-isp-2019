package gheorghita.vlad.lab4.ex6;

public class Test {
    public static void main(String[] args) {
        Circle cerc = new Circle("albastru", true, 2.7);
        Square patrat = new Square("Visiniu", false, 5.3, 5.3);
        System.out.print(patrat.toString() + "\n" + cerc.toString());
    }
}
