package gheorghita.vlad.lab4.ex6;

public class Shape {
    private String color = "red";
    private boolean fiiled = true;

    public Shape() {
    }

    public Shape(String color, boolean fiiled) {
        this.color = color;
        this.fiiled = fiiled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFiiled(boolean fiiled) {
        this.fiiled = fiiled;
    }

    public boolean isFiiled() {
        return fiiled;
    }


    public String toString() {
        return "A Shape with color of " + color + "and filled: " + fiiled;
    }
}
