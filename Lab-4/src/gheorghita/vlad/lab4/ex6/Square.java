package gheorghita.vlad.lab4.ex6;

public class Square extends Rectangle {
    public Square() {
    }

    public Square(double width, double length) {
        super(width, length);
    }

    public Square(String color, boolean fiiled, double width, double length) {
        super(color, fiiled, width, length);
    }


    public void setWidth(double width) {
        super.setWidth(width);
    }


    public void setLength(double length) {
        super.setLength(length);
    }

    public String toString() {
        return "A square with side= " + super.getLength() + ", which is a subclass of " + super.toString();
    }
}
