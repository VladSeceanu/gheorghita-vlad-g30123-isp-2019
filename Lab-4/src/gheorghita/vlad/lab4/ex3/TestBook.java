package gheorghita.vlad.lab4.ex3;

import gheorghita.vlad.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author a = new Author("Ligia", "ceva@hotmail.com", 'f');
        Book b = new Book("Carte", a, 100);
        Book b2 = new Book("Carticica", a, 200, 20);
        b.setQtyInStock(5);
        System.out.println(b.getAuthor().toString() + ' ' + b.getName() + ' ' + b.getPrice() + ' ' + b.getQtyInStock());

    }
}
