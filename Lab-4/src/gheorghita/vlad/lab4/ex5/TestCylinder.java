package gheorghita.vlad.lab4.ex5;

import gheorghita.vlad.lab4.ex1.Circle;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder cilindru1 = new Cylinder(2.5);
        Cylinder cilindru2 = new Cylinder(1.7, 3.7);
        System.out.println("Height for cilindrul1: " + cilindru1.getHeight());
        System.out.println("Height for cilindrul2: " + cilindru2.getHeight());
        System.out.println("Volume for cilindru1l: " + cilindru1.getVolume() + " and cilindrul2: " + cilindru2.getVolume());
    }
}
