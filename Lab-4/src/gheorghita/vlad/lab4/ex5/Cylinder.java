package gheorghita.vlad.lab4.ex5;

import java.lang.Math;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {
    }

    public Cylinder(double radius){
        super(radius);
    }

    public Cylinder(double radius, double height){
        super(radius);
        this.height=height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume(){
        return Math.PI*getRadius()*getRadius()*height;
    }

    @Override
    public double getArea() {
        return 2*Math.PI*getRadius()*height;
    }
}
