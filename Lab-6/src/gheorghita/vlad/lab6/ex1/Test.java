package gheorghita.vlad.lab6.ex1;

public class Test {
    public static void main(String[] args) {
        BankAccount cont1 = new BankAccount();
        BankAccount cont2 = new BankAccount();
        cont1.deposit(257.25);
        cont2.deposit(196.76);
        BankAccount cont3 = cont1;
        System.out.println("cont1 == cont2 ?   " + cont1.equals(cont2) + "  with hashCode: " + cont1.hashCode() + "/" + cont2.hashCode());
        System.out.println("cont2 == cont3 ?   " + cont2.equals(cont3) + "  with hashCode: " + cont2.hashCode() + "/" + cont3.hashCode());
        System.out.println("cont1 == cont3 ?   " + cont1.equals(cont3) + "  with hashCode: " + cont1.hashCode() + "/" + cont3.hashCode());
        cont3 = new BankAccount();
        System.out.println("cont1 == cont3 ?   " + cont1.equals(cont3) + "  with hashCode: " + cont1.hashCode() + "/" + cont3.hashCode());
    }
}
