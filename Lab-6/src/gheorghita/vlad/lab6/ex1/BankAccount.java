package gheorghita.vlad.lab6.ex1;

public class BankAccount {
    private String owner;
    private double balance;

    public void withdraw(double amount) {
        balance = balance - amount;
        System.out.println("New balance: " + balance);
    }

    public void deposit(double amount) {
        balance = balance + amount;
        System.out.println("New balance: " + balance);
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public int hashCode() {
        return super.hashCode();
    }
}

