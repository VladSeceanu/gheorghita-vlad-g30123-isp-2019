package gheorghita.vlad.lab6.ex4;


import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args){
        Dictionary dictionary = new Dictionary();
        boolean goOn = true;
        int nr;

        String cuvant;
        String definitie;
        System.out.println("1.Add Word\n" +
                "2.Get Definition\n" +
                "3.Show All Words\n" +
                "4.Show All Definitions\n" +
                "5.Exit");
        while (goOn) {
            Scanner in = new Scanner(System.in);
            System.out.print("Enter a number: ");
            nr = in.nextInt();
            switch (nr) {
                case 1:
                    System.out.print("Enter word: ");
                    cuvant=in.next();
                    Word word = new Word(cuvant);
                    System.out.print("Enter definition: ");
                    definitie=in.next();
                    Definition definition = new Definition(definitie);
                    dictionary.addWord(word, definition);
                    dictionary.getAllDefinitions();
                    break;
                case 2:
                    System.out.print("Enter word: ");
                    cuvant=in.nextLine();
                    word = new Word(cuvant);
                    dictionary.getDefinition(word);
                    break;
                case 3:
                    dictionary.getAllWords();
                    break;
                case 4:
                    dictionary.getAllDefinitions();
                    break;
                case 5:
                    goOn = false;
                default:
                    System.out.println("Wrong number, chose again!");
                    break;
            }
            System.out.println();
        }
    }
}
