package gheorghita.vlad.lab6.ex4;

import java.util.HashMap;

public class Dictionary {
    private HashMap<Word, Definition> dictionary = new HashMap<Word, Definition>();

    public void addWord(Word w, Definition d) {
        dictionary.put(w, d);
    }

    public String getDefinition(Word w) {
        return dictionary.get(w).getDescription();
    }

    public void getAllWords() {
        for (Word word : dictionary.keySet()) {
            System.out.println(word.getName());
        }
    }

    public void getAllDefinitions(){
        for(Definition definition:dictionary.values()){
            System.out.println(definition.getDescription());
        }
    }
}
