package gheorghita.vlad.lab6.ex2;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        BankAccount cont = new BankAccount(owner, balance);
        accounts.add(cont);
        //SAU
        /*
        accounts.add(new BankAccount(owner,balance));

        */
    }

    public void printAccounts() {
        Collections.sort(accounts, new Comparator<BankAccount>() {
            public int compare(BankAccount o1, BankAccount o2) {
                if (o1.getBalance() > o2.getBalance()) return 1;
                if (o1.getBalance() < o2.getBalance()) return -1;
                return 0;
            }
        });
        for (BankAccount cont : accounts) {
            System.out.println(cont.toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount cont : accounts) {
            if (cont.getBalance() >= minBalance && cont.getBalance() <= maxBalance)
                System.out.println(cont.toString());
        }
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount cont : accounts) {
            if (cont.getOwner() == owner) return cont;
        }
        return null;
    }

    public List<BankAccount> getAllAccounts() {
        return accounts;
    }
}