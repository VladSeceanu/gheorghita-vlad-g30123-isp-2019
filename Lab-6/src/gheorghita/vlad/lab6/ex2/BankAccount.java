package gheorghita.vlad.lab6.ex2;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        balance = balance - amount;
        System.out.println("New balance: " + balance);
    }

    public void deposit(double amount) {
        balance = balance + amount;
        System.out.println("New balance: " + balance);
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}

