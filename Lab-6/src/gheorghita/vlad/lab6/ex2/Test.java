package gheorghita.vlad.lab6.ex2;

import java.util.List;

public class Test {
    public static void main(String[] args) {
        Bank banca = new Bank();
        BankAccount cont;
        banca.addAccount("Mihaela", 587.78);
        banca.addAccount("Mircea", 276.57);
        banca.addAccount("Ana", 1343.48);
        banca.addAccount("Flaviu", 957.02);
        banca.printAccounts();
        System.out.println();
        banca.printAccounts(500, 1000);
        System.out.println();
        System.out.println(banca.getAccount("Mircea").toString());
        System.out.println();
        List<BankAccount> conturi= banca.getAllAccounts();
        for(BankAccount contul:conturi){
            System.out.println(contul.toString());
        }
    }
}
