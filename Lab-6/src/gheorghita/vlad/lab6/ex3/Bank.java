package gheorghita.vlad.lab6.ex3;

import java.util.TreeSet;

public class Bank {
    private TreeSet<BankAccount> accounts = new TreeSet<>();

    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        for (BankAccount cont : accounts) {
            System.out.println(cont.toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount cont : accounts) {
            if (cont.getBalance() > minBalance && cont.getBalance() < maxBalance)
                System.out.println(cont.toString());
        }
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount cont : accounts) {
            if (cont.getOwner() == owner) return cont;
        }
        return null;
    }

    public TreeSet<BankAccount> getAllAccounts() {
        return accounts;
    }
}
