package gheorghita.vlad.lab6.ex3;

public class BankAccount implements Comparable {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        balance = balance - amount;
        System.out.println("New balance: " + balance);
    }

    public void deposit(double amount) {
        balance = balance + amount;
        System.out.println("New balance: " + balance);
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    public int compareTo(Object o) {
        BankAccount cont = (BankAccount) o;
        if (balance > cont.getBalance()) return 1;
        if (balance < cont.getBalance()) return -1;
        return 0;
    }
}

