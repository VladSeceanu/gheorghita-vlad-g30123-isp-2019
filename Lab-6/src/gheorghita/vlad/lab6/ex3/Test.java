package gheorghita.vlad.lab6.ex3;

import java.util.TreeSet;

public class Test {
    public static void main(String[] args) {
        Bank banca = new Bank();
        BankAccount cont;
        banca.addAccount("Mihaela", 587.78);
        banca.addAccount("Mircea", 276.57);
        banca.addAccount("Ana", 1343.48);
        banca.addAccount("Flaviu", 957.02);
        banca.printAccounts();
        System.out.println();
        banca.printAccounts(500, 1000);
        System.out.println();
        System.out.println(banca.getAccount("Mircea").toString());
        System.out.println();
        TreeSet<BankAccount> conturi = banca.getAllAccounts();
        for(BankAccount contul:conturi){
            System.out.println(contul.toString());
        }
    }
}
