package gheorghita.vlad.lab3.ex4;

import java.lang.Math;

public class MyPoint {
    private int x, y;

    public MyPoint() {
        x = 0;
        y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void getX() {
        System.out.println("x= " + x);
    }

    public void getY() {
        System.out.println("y= " + y);
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "(" + x + "," + y + ")";
    }

    public double distance(int x, int y) {
        return Math.sqrt((x - this.x) ^ 2 + (y - this.y) ^ 2);
    }

    public double distance(MyPoint pct) {
        return Math.sqrt((x - pct.x) ^ 2 + (y - pct.y) ^ 2);
    }
}
