package gheorghita.vlad.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint pct1 = new MyPoint();
        MyPoint pct2 = new MyPoint(2, 3);
        pct1.getX();
        pct1.getY();
        pct1.setXY(7, 9);
        System.out.println("Coordinates of pct1: " + pct1.toString());
        System.out.println("Coordinates of pct2: " + pct2.toString());
        System.out.println("Distance between pct2 and (10,12): " + pct2.distance(10, 12));
        System.out.println("Distance between pct1 and pct2: " + pct1.distance(pct2));
    }
}
