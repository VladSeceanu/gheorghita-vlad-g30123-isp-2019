package gheorghita.vlad.lab3.ex1;

public class Robot {
    private int x;

    public Robot() {
        this.x=1;
    }

    public void change(int k) {
        if (k >= 1) this.x += k;
    }

    public String toString() {
        int number = this.x;
        return Integer.toString(number);
    }
}
