package gheorghita.vlad.lab3.ex1;
import java.util.*;

public class TestRobot {
    public static void main(String[] args) {
        Robot robotel = new Robot();
        Scanner in=new Scanner(System.in);
        int pozNou;
        System.out.println("Pozitia: " + robotel.toString());
        System.out.print("Introdu deplasarea: ");
        pozNou=in.nextInt();
        robotel.change(pozNou);
        System.out.println("Pozitia noua: " + robotel.toString());
    }
}
