package gheorghita.vlad.lab3.ex2;

import static java.lang.Math.PI;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    public Circle() {
        this.radius = 3.7;
        this.color = "blue";
    }

    public Circle(double x, String y) {
        this.radius = x;
        this.color = y;
    }

    public double getRadius() {
        double aux = this.radius;
        return aux;
    }

    public double getArea() {
        double aux = this.radius;
        return PI * (aux * aux);
    }
}
