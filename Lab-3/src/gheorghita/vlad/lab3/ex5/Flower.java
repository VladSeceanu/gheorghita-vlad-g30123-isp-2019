package gheorghita.vlad.lab3.ex5;

public class Flower {
    int petal;
    static int nr = 0;

    Flower() {
        System.out.println("Flower has been created!");
    }

    public void count() {
        nr++;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++) {
            Flower f = new Flower();
            garden[i] = f;
            f.count();
        }
        System.out.println("Number of objects constructed: " + Flower.nr);
    }
}