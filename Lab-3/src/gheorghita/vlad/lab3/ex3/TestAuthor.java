package gheorghita.vlad.lab3.ex3;

public class TestAuthor {
    public static void main(String[] args) {
        Author autor1 = new Author();
        Author autor2 = new Author("Daniela", "Daniela@yahoo.com", 'f');
        System.out.println(autor1.getName());
        System.out.println(autor2.getName());
        System.out.println(autor1.getEmail());
        System.out.println(autor2.getEmail());
        System.out.println(autor1.getGender());
        System.out.println(autor2.getGender());
        autor2.setEmail("Daniela28@yahoo.com");
        System.out.println(autor1.getEmail());
        System.out.println(autor2.getEmail());
    }
}
