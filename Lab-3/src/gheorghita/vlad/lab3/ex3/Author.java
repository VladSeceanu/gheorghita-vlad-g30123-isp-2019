package gheorghita.vlad.lab3.ex3;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author() {
        this.name = "Andrei";
        this.email = "Andrei@yahoo.com";
        this.gender = 'm';
    }

    public Author(String nume, String mail, char gen) {
        this.name = nume;
        this.email = mail;
        this.gender = gen;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String nou) {
        this.email = nou;
    }

    public String toString() {
        return name + " (" + gender + ") at " + email;
    }
}
