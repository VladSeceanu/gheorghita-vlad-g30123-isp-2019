package gheorghita.vlad.lab7.ex2;


import java.io.*;
import java.util.*;

public class Counter {
    public static void main(String args[]) throws IOException {

        char ch;
        BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.println("Give the character:");
        ch = stdin.readLine().charAt(0);

        int nr = 0;

        //citire din fisier
        BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\Vlad\\Desktop\\UTC-N\\Laboratoare\\ISP(Java)\\gheorghita-vlad-g30123-isp-2019\\Lab-7\\src\\gheorghita\\vlad\\lab7\\ex2\\data.txt"));
        String s;
        while ((s = in.readLine()) != null) {
            char[] lin = s.toCharArray();
            for (char c : lin) {
                if (c == c) {
                    nr++;
                }
            }
        }
        in.close();
        System.out.println("The character " + ch + " appears " + nr + " times");
    }

}
