package gheorghita.vlad.lab5.ex3;


public class Controller {
    public TemperatureSensor tempSensor = new TemperatureSensor();
    public LightSensor lightSensor = new LightSensor();

    public void control() throws InterruptedException {
        int sec = 0;
        while (sec <= 20) {
            System.out.println("Temperature: " + tempSensor.readValue() + "\u00B0C");
            System.out.println("Lightning: " + lightSensor.readValue() + " Lumens");
            Thread.sleep(1000);
            sec++;
        }
    }
}
