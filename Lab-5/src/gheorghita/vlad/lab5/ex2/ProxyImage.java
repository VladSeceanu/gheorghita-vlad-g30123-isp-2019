package gheorghita.vlad.lab5.ex2;

import java.util.Scanner;

public class ProxyImage implements Image {

    private Image image;
    private String fileName;

    private Scanner in = new Scanner(System.in);

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    public void display() {
        if (fileName == "Real") image = new RealImage(fileName);
        else if (fileName == "Rotated") image = new RotatedImage(fileName);
        image.display();
    }
}
