package gheorghita.vlad.lab9.ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Game extends JFrame {
JButton button[] = new JButton[9];
    static public int turn = 0;
    JTextArea text;
    public boolean win = false;

    Game(){
        setTitle("X & O");
        setSize(500,500);
        init();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void init(){
        setLayout(new GridLayout(4,3));
        for(int i = 0; i < 9; i++){
            button[i] = new JButton("");
            add(button[i]);
            button[i].addActionListener(new Push());
        }
        text = new JTextArea();
        text.setBounds(50,450, 100,100);
        add(text);

    }

    public String verfyRows(){
        int row1 = 0, row2 = 0, row3 = 0;

        for(int i = 0; i < 2; i++){
            if(button[i].getText().equals(button[i + 1].getText()) && button[i].getText() != "") row1++;
            else row1 = 0;
            if(button[i + 3].getText().equals(button[i + 4].getText()) && button[i + 3].getText() != "") row2++;
            else row2 = 0;
            if(button[i + 6].getText().equals(button[i + 7].getText()) && button[i + 6].getText() != "") row3++;
            else row3 = 0;

        }
        if(row1 == 2) return button[0].getText();
        if(row2 == 2) return button[3].getText();
        if(row3 == 2) return button[6].getText();
        return "no player has won yet";
    }

    public String verifyColumns(){
        int columns = 0;
        for(int i = 0; i < 3; i++){
            if(button[i].getText().equals(button[i + 3].getText()) && button[i].getText() != "") columns++;
            else columns = 0;
            if(button[i + 3].getText().equals(button[i + 6].getText()) && button[i + 3].getText() != "") columns ++;
            else columns = 0;
            if(columns >= 2) return button[i].getText();

        }
        return "no player has won yet";
    }

    public String verifyDiag(){
        int diag1 = 0, diag2 = 0;
        if(button[0].getText().equals(button[4].getText())) diag1++;
        if(button[4].getText().equals(button[8].getText())) diag1++;

        if(button[2].getText().equals(button[4].getText())) diag2++;
        if(button[4].getText().equals(button[6].getText())) diag2++;

        if(diag1 == 2 || diag2 == 2) return button[4].getText();
        return "no player has won yet";
    }

    public boolean verifyWritten(){
        for(int i = 0; i < 9; i++)
            if(button[i].getText().equals("")) return false;
        return true;
    }


    public class Push implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton but = (JButton)e.getSource();
            if(but.getText().equals("X") || but.getText().equals("O")){}
            else {
                turn++;
                if (turn % 2 != 0) {
                    but.setText("X");
                } else but.setText("O");
                if ((verfyRows().equals("X") || verifyColumns().equals("X") || verifyDiag().equals("X")) && win == false) {
                    win = true;
                    Game.this.text.setText("Player X has won!");
                } else if ((verfyRows().equals("O") || verifyColumns().equals("O") || verifyDiag().equals("O")) && win == false){
                    Game.this.text.setText("Player O has won");
                    win = true;
                }
                else if (verifyWritten() && win == false) Game.this.text.setText("It's a tie!");
            }
        }
    }

    static public void main(String args[]){
        new Game();
    }
}
