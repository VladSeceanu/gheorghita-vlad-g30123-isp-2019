package Gheorghita.Vlad.lab2.ex5;
import java.util.Random;

public class bubbleSort {
   private void bubble(int arr[]){
        int n = arr.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (arr[j] > arr[j+1])
                {
                    // swap arr[j+1] and arr[i]
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
    }
    public static void main(String[] args){
        Random r = new Random();
        bubbleSort ob=new bubbleSort();
        int[] arr = new int[10];
        for(int i = 0 ; i < arr.length ; i++) arr[i] = r.nextInt(50);
        System.out.println("Unsorted:");
        for(int el : arr) System.out.print(el+" ");
        System.out.println();
        ob.bubble(arr);
        System.out.println("Sorted:");
        for(int el : arr) System.out.print(el+" ");
    }
}
