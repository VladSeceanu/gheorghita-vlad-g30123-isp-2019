package Gheorghita.Vlad.lab2.ex6;
import java.util.Scanner;

public class Factorial {
    private int non_recursive(int a){
        int p = 1;
        for(int i = 1 ; i <= a ; i++) p *= i;
        return p;
    }
    private int recursive(int a){
        if(a < 1) return 1;
        else return a * recursive(a - 1);
    }
    public static void main(String[] args){
        Factorial Ob = new Factorial();
        Scanner in = new Scanner(System.in);
        System.out.print("Numarul: ");
        int n = in.nextInt();
        System.out.print("1: recursiv ; 2: nerecursiv...    ");
        int var = in.nextInt();
        switch(var)
        {
            case 1: n = Ob.recursive(n); break;
            case 2: n = Ob.non_recursive(n); break;
            default: n = Ob.recursive(n); break;
        }
        System.out.println(n);
    }
}
