package Gheorghita.Vlad.lab2.ex7;

import java.util.Scanner;
import java.util.Random;

public class Guess {
    public static void main(String[] args) {
        Random ran = new Random();
        Scanner in = new Scanner(System.in);
        int numarul = ran.nextInt(10);
        int a;
        for (int i = 1; i <= 3; i++) {
            a = in.nextInt();
            if (a > numarul) System.out.println("Wrong answer, your number is too high!");
            else if (a < numarul) System.out.println("Wrong answer, your number is too low!");
            else {
                System.out.println("You win!");
                break;
            }
            if (i == 3) System.out.println("You lost! The number was: "+ numarul);
        }
    }
}
