package Gheorghita.Vlad.lab2.ex4;
import java.util.Scanner;

public class MaxElem {
    public static void main(String[] args){
        Scanner in= new Scanner(System.in);
        int n= in.nextInt();
        int[] array= new int[n];
        for(int i=0; i<array.length;i++) array[i] = in.nextInt();
        int max=array[0];
        for(int el : array) if(el>max) max=el;
        System.out.println(max);
    }
}
