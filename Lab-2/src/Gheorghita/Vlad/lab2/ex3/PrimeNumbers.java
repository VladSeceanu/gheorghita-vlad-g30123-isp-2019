package Gheorghita.Vlad.lab2.ex3;
import java.util.Scanner;

public class PrimeNumbers {
  private static boolean prim(int x)
    {
        if(x<2) return false;
        if(x>3 && x%2==0) return false;
        for(int i=3;i<=x/2;i+=2)
        {
            if(x%i==0) return false;
        }
        return true;
    }
    public static void main(String[] args){
        Scanner in= new Scanner(System.in);
        String raspuns="";
        int A = in.nextInt();
        int B = in.nextInt();
        int nr=0;
        for(int i=A;i<=B;i++)
            if (prim(i)) {
                raspuns  =raspuns+i+" ";
                nr++;
            }
        System.out.println(raspuns+" "+ nr);
    }
}
